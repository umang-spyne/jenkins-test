pipeline {
    agent any

    stages {
        stage('Hello') {
            steps {
                script {
                    echo 'Hello World!'
                    git branch: 'main', url: 'https://gitlab.com/umang-spyne/jenkins-test'
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    echo 'Building....'
                    npm run build
                }
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
        stage('Test') {
            steps {
                script {
                    echo 'Testing....'
                    npm run test
                }
            }
        }
        stage('Release') {
            steps {
                echo 'Releasing....'
            }
        }
        stage('Shut Down') {
            steps {
                echo 'Shutting down....'
            }
        }
    }
}
